package com.ycnet.springapp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

/**
 * 开发环境配置类
 */
@Configuration
@Profile("dev")
@PropertySource(value = {"classpath:/profiles/dev.properties" }, ignoreResourceNotFound = true)
public class ProfileConfig__Dev {
	
}
