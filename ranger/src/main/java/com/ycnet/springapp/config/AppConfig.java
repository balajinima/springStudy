/**
 * 
 */
package com.ycnet.springapp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author ycnet
 * http://docs.spring.io/spring/docs/4.2.x/javadoc-api/
 */
//用于配置Spring的相关信息
@Configuration
//在basePackages指定的目录下扫描被@Controller、@Service、@Component等注解注册的组件,使用REGEX excludeFilter 排除扫描com.ycnet.springapp.web.*，
@ComponentScan(basePackages = { "com.ycnet.springapp" }, excludeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = { "com.ycnet.springapp.web.*" }))
//加载指定的配置文件，配置文件内容会加载入Environment中等待调用
@PropertySource(value = { "classpath:application.properties" })
//启用任务调度
@EnableScheduling
//启用切面自动代理，用于AOP
@EnableAspectJAutoProxy
//使用@EnableCaching激活了缓存，需要定义CacheManager  bean
@EnableCaching
public class AppConfig {

	@Autowired
	private Environment env;

	@Bean
	public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public CacheManager cacheManager() {
		return new ConcurrentMapCacheManager();
	}

//	@Bean
//	public JavaMailSenderImpl javaMailSenderImpl() {
//		JavaMailSenderImpl mailSenderImpl = new JavaMailSenderImpl();
//		mailSenderImpl.setHost(env.getProperty("smtp.host"));
//		mailSenderImpl.setPort(env.getProperty("smtp.port", Integer.class));
//		mailSenderImpl.setProtocol(env.getProperty("smtp.protocol"));
//		mailSenderImpl.setUsername(env.getProperty("smtp.username"));
//		mailSenderImpl.setPassword(env.getProperty("smtp.password"));
//
//		Properties javaMailProps = new Properties();
//		javaMailProps.put("mail.smtp.auth", true);
//		javaMailProps.put("mail.smtp.starttls.enable", true);
//
//		mailSenderImpl.setJavaMailProperties(javaMailProps);
//
//		return mailSenderImpl;
//	}
}
