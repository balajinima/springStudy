package com.ycnet.springapp.web.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;


public class WebContextInitializerClasse implements ApplicationContextInitializer<ConfigurableApplicationContext> {

	private static final Logger logger = LoggerFactory.getLogger(WebContextInitializerClasse.class);

	@Override
	public void initialize(ConfigurableApplicationContext applicationContext) {

		String profile;
		// 取用户环境变量
		profile = System.getenv("SpringProfilesActive");

		if (profile == null) {// 默认是开发环境
			System.err.println("默认开启使用develop配置，若需要改变请配置环境变量[SpringProfilesActive]");
			profile = "dev";
		}
		logger.info("Active spring profile: {}", profile);
		applicationContext.getEnvironment().setActiveProfiles(profile);
	}
}